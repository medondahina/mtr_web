import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes,RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminComponent } from './admin/admin.component';
export const routes:Routes=[
  {path:'admin',component:AdminComponent,children:[
    {path:'',pathMatch:'full',redirectTo:'dashboard'},
    {path:'dashboard',component:AdminDashboardComponent}
  ]}
 
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AdminComponent,AdminDashboardComponent]
})
export class AdminModule { }
