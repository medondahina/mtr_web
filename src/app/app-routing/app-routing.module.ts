import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminModule } from '../home/admin/admin.module';
import { HomeComponent } from '../home/home.component';
// export function loadAdminModule() {
//   return AdminModule;
// }
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent },
  { path: 'admin', loadChildren: '../home/admin/admin.module#AdminModule' }
]
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
